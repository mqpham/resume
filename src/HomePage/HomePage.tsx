import React from 'react';
import { Box, makeStyles, Typography, Grid } from '@material-ui/core/';
import lightBackground from '../assets/lightBackground.jpg';
import darkBackground from '../assets/darkBackground.jpg';
import Gitlab from '../assets/Gitlab.png';
import Linkedin from '../assets/Linkedin.png';

const useStyles = makeStyles((theme) => ({
  background: {
    position: 'relative',
    height: '850px',
    minHeight: '500px',
    width: '100%',
    backgroundImage: `url(${
      theme.palette.type === 'dark' ? darkBackground : lightBackground
    })`,
    backgroundSize: 'cover !important',
    webkitBackgroundSize: 'cover !important',
    textAlign: 'center',
    overflow: 'hidden'
  },
  bannerText: {
    fontWeight: 'bold'
  },
  imageIcon: {
    paddingRight: theme.spacing(2)
  }
}));

function HomePage(): React.ReactElement {
  const classes = useStyles();

  return (
    <Box display="flex" className={classes.background}>
      <Grid container justify="center" alignContent="center">
        <Grid item xs={12}>
          <Typography
            color="inherit"
            align="center"
            variant="h1"
            className={classes.bannerText}
          >
            My Pham
          </Typography>
          <Typography variant="h3" className={classes.bannerText}>
            Software Engineer
          </Typography>
          <Box mt={2}>
            <a href="https://www.gitlab.com/mqpham">
              <img
                alt="Gitlab"
                className={classes.imageIcon}
                height="80"
                width="80"
                src={Gitlab}
              />
            </a>
            <a href="https://www.linkedin.com/pub/my-pham/97/858/a09">
              <img alt="Linkedin" height="80" width="80" src={Linkedin} />
            </a>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
}

export default HomePage;
