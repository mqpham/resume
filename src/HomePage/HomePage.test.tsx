import React from 'react';
import { render, screen } from '@testing-library/react';
import HomePage from './HomePage';

test('renders the home page', () => {
  render(<HomePage />);
  expect(screen.getByText('My Pham')).not.toBeNull();
});
