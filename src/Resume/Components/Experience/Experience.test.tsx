import React from 'react';
import { render, screen } from '@testing-library/react';
import Experience from './Experience';

describe('<Experience', () => {
  it('should render without error', () => {
    render(<Experience />);

    expect(screen.getByText('Experience')).not.toBeNull();
  });

  it('should show in responsive layout', () => {
    window.matchMedia =
      window.matchMedia ||
      function () {
        return {
          matches: true,
          addListener() {},
          removeListener() {}
        };
      };

    render(<Experience />);

    expect(screen.getByText('Experience')).not.toBeNull();
  });
});
