import React from 'react';
import {
  Box,
  makeStyles,
  Typography,
  Grid,
  useMediaQuery,
  useTheme
} from '@material-ui/core/';

const useStyles = makeStyles((theme) => ({
  education: {
    paddingTop: '96px',
    paddingBottom: '96px',
    overflow: 'hidden'
  },
  row: {
    width: '96%',
    maxWidth: '1020px',
    margin: '0 auto'
  }
}));

function Experience(): React.ReactElement {
  const classes = useStyles();

  const theme = useTheme();

  const matches = useMediaQuery(theme.breakpoints.up('sm'));

  return (
    <Box className={classes.education}>
      <Grid
        container
        justify="center"
        alignContent="center"
        className={classes.row}
        direction={matches ? 'row' : 'column'}
      >
        <Grid item xs={3}>
          <Typography variant="h6">Experience</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography align="left" variant="h6">
            TECHNICAL PROJECTS:
          </Typography>
          <Typography align="left" variant="subtitle2">
            The Media Trust / McLean Virginia / November 2021 - Present
          </Typography>
          <Typography align="left" variant="subtitle2">
            Ad Scanner Platform:
          </Typography>
          <Typography align="left" variant="body1">
            Developed with Typescript/Javascript, HTML and CSS for a web page
            that allows clients to scan their website for internet
            advertisements to be filtered into different categories to be
            prohibited or blocked from users to see. Collaborated with PHP
            engineers and UX designers to modernize the Ad Scanning platform by
            converting previous version web pages and newly designed ones into
            React. Used backend REST API endpoints from PHP actions to request
            data and used React/Redux for the frontend.
          </Typography>
          <Typography align="left" variant="subtitle2">
            Radical Convergence / Reston Virginia / February 2021 - November
            2021
          </Typography>
          <Typography align="left" variant="subtitle2">
            Render Platform:
          </Typography>
          <Typography align="left" variant="body1">
            Developed with Typescript/Javascript, HTML and CSS for a web page
            database that stored images of 3D created models in house by
            artists. Created user interface for managing users with AWS cognito
            service. Used NodeJS to implement the backend CRUD services and
            endpoints. Wrote frontend code with React and Redux. Worked on
            converting Selenium tests by creating Cypress end to end tests to
            run when a finished version was deployed.
          </Typography>
          <Typography align="left" variant="subtitle2">
            Parsons Corporation / Centreville Virginia / July 2017 - February
            2021
          </Typography>
          <Typography align="left" variant="subtitle2">
            Parsons Pure Magic Platform:
          </Typography>
          <Typography align="left" variant="body1">
            Developed with Typescript/Javascript, HTML and CSS a command and
            control single page web application. Took wireframes and implemented
            components in both React and AngularJS languages. Wrote unit tests
            with the react-testing-library and jest to have code coverage on
            React components. Took on tasks of integrating Django/Python API
            with frontend by using the data models provided to display each JSON
            object in a user friendly manner. Worked on creating a Gitlab CI/CD
            pipeline with the automation testing framework Cypress.io and wrote
            test scripts to minimize manual tests.
          </Typography>
          <Typography align="left" variant="subtitle2">
            Resume Website (mqpham.gitlab.io/resume):
          </Typography>
          <Typography align="left" variant="body1">
            Implemented a static website integrated with Gitlab CICD pipeline to
            test, build and deploy to a publicly accessible Gitlab url. Built
            with React, Typescript and uses components from Material-UI library.
            Unit tests are written with jest for coverage reports. Dark mode and
            layout responsiveness for website and mobile view were taken into
            consideration. Repository to view code:
            https://gitlab.com/mqpham/resume
          </Typography>
        </Grid>
      </Grid>
    </Box>
  );
}

export default Experience;
