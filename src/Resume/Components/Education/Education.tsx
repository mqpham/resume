import React from 'react';
import {
  Box,
  makeStyles,
  Typography,
  Grid,
  useMediaQuery,
  useTheme,
} from '@material-ui/core/';

const useStyles = makeStyles((theme) => ({
  education: {
    paddingTop: '96px',
    paddingBottom: '96px',
    overflow: 'hidden',
  },
  row: {
    width: '96%',
    maxWidth: '1020px',
    margin: '0 auto',
  },
}));

function Education(): React.ReactElement {
  const classes = useStyles();

  const theme = useTheme();

  const matches = useMediaQuery(theme.breakpoints.up('sm'));

  return (
    <Box className={classes.education}>
      <Grid container justify="center" alignContent="center" className={classes.row} direction={matches ? 'row' : 'column'}>
        <Grid item xs={3}>
          <Typography variant="h6">
            Education
          </Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography color="inherit" variant="h5">
            Syracuse University
          </Typography>
          <Typography align="left" variant="body1">
            College of Engineering and Computer Science
          </Typography>
          <Typography align="left" variant="body1">
            Bachelor of Science, May 2016
          </Typography>
          <Typography align="left" variant="body1">
            Major: Computer Science
          </Typography>
          <Typography align="left" variant="body1">
            GPA 3.4
          </Typography>
          <Typography align="left" variant="body1">
            Master of Science, May 2017
          </Typography>
          <Typography align="left" variant="body1">
            Major: Computer Science
          </Typography>
          <Typography align="left" variant="body1">
            GPA 3.4
          </Typography>
        </Grid>
      </Grid>
    </Box>
  );
}

export default Education;
