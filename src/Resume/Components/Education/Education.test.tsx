import React from 'react';
import { render, screen } from '@testing-library/react';
import Education from './Education';

describe('<Education', () => {
  it('should render without error', () => {
    render(<Education />);

    expect(screen.getByText('Education')).not.toBeNull();
  });

  it('should show in responsive layout', () => {
    window.matchMedia =
      window.matchMedia ||
      function () {
        return {
          matches: true,
          addListener() {},
          removeListener() {}
        };
      };

    render(<Education />);

    expect(screen.getByText('Education')).not.toBeNull();
  });
});
