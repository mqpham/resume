import React from 'react';
import { render, screen } from '@testing-library/react';
import Skills from './Skills';

describe('<Skills', () => {
  it('should render without error', () => {
    render(<Skills />);

    expect(screen.getByText('SKILLS/TECHNOLOGY:')).not.toBeNull();
  });

  it('should show in responsive layout', () => {
    window.matchMedia =
      window.matchMedia ||
      function () {
        return {
          matches: true,
          addListener() {},
          removeListener() {}
        };
      };

    render(<Skills />);

    expect(screen.getByText('SKILLS/TECHNOLOGY:')).not.toBeNull();
  });
});
