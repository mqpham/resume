import React from 'react';
import {
  Box,
  makeStyles,
  Typography,
  Grid,
  useMediaQuery,
  useTheme
} from '@material-ui/core/';

const useStyles = makeStyles((theme) => ({
  education: {
    paddingTop: '96px',
    paddingBottom: '96px',
    overflow: 'hidden'
  },
  row: {
    width: '96%',
    maxWidth: '1020px',
    margin: '0 auto'
  }
}));

function Skills(): React.ReactElement {
  const classes = useStyles();

  const theme = useTheme();

  const matches = useMediaQuery(theme.breakpoints.up('sm'));

  return (
    <Box className={classes.education}>
      <Grid
        container
        justify="center"
        alignContent="center"
        className={classes.row}
        direction={matches ? 'row' : 'column'}
      >
        <Grid item xs={3}>
          <Typography variant="h6">Skills</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography align="left" variant="h6">
            SKILLS/TECHNOLOGY:
          </Typography>
          <Typography align="left" variant="subtitle2">
            Software:
          </Typography>
          <Typography align="left" variant="body1">
            React, JavaScript, Typescript, HTML, CSS, Jest, npm, NodeJS,
            AngularJS, Cypress.io, Docker, Linux operating systems.
          </Typography>
          <Typography align="left" variant="subtitle2">
            Tools:
          </Typography>
          <Typography align="left" variant="body1">
            PyCharm, Webstorm, IntelliJ, Jira, Confluence, Bitbucket, Gitlab,
            Git, ESLint, Chrome Dev Tools, Sublime, Microsoft Visual Studio,
            Microsoft Office
          </Typography>
          <Typography align="left" variant="subtitle2">
            Other:
          </Typography>
          <Typography align="left" variant="body1">
            Scrum, Agile, Code reviews, Sprint planning, UML diagrams, able to
            communicate in English and Vietnamese.
          </Typography>
        </Grid>
      </Grid>
    </Box>
  );
}

export default Skills;
