import React from 'react';
import { render, screen } from '@testing-library/react';
import AboutMe from './AboutMe';

describe('AboutMe component', () => {
  it('should render without error', () => {
    render(<AboutMe />);
    expect(screen.getByText('About Me')).not.toBeNull();
  });

  it('should show in responsive layout', () => {
    window.matchMedia =
      window.matchMedia ||
      function () {
        return {
          matches: true,
          addListener() {},
          removeListener() {}
        };
      };

    render(<AboutMe />);

    expect(screen.getByText('About Me')).not.toBeNull();
  });
});
