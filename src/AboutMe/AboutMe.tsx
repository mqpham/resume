import React from 'react';
import {
  Box,
  makeStyles,
  Typography,
  Grid,
  Avatar,
  useMediaQuery,
  useTheme
} from '@material-ui/core/';
import photo from '../assets/photo.jpg';

const useStyles = makeStyles((theme) => ({
  about: {
    paddingTop: '96px',
    paddingBottom: '96px',
    overflow: 'hidden'
  },
  row: {
    width: '96%',
    maxWidth: '1020px',
    margin: '0 auto'
  },
  largeIcon: {
    width: theme.spacing(10),
    height: theme.spacing(10)
  },
  align: {
    alignSelf: 'center',
    marginBottom: theme.spacing(2)
  }
}));

function AboutMe(): React.ReactElement {
  const classes = useStyles();

  const theme = useTheme();

  const matches = useMediaQuery(theme.breakpoints.up('sm'));

  return (
    <Box className={classes.about}>
      <Grid
        container
        justify="center"
        alignContent="center"
        className={classes.row}
        direction={matches ? 'row' : 'column'}
      >
        <Grid
          item
          xs={matches ? 3 : 12}
          className={!matches ? classes.align : undefined}
        >
          <Avatar className={classes.largeIcon} src={photo} />
        </Grid>
        <Grid item xs={6}>
          <Typography color="inherit" variant="h6">
            About Me
          </Typography>
          <Typography variant="subtitle1">
            I am a software engineer who was born and raised in Syracuse, New
            York. I attended Syracuse University where I received my
            Bachelor&#39;s and Master&#39;s Degree in Computer Science through
            the 5 year accelerated program. I am currently located in Virginia
            working as a frontend engineer.
          </Typography>
        </Grid>
      </Grid>
    </Box>
  );
}

export default AboutMe;
