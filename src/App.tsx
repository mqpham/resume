import React, { useState, useRef } from 'react';
import {
  Grid,
  MuiThemeProvider,
  Toolbar,
  AppBar,
  IconButton,
  Tooltip,
  createMuiTheme,
  Paper,
  makeStyles,
  Button,
  Divider,
  Box,
  responsiveFontSizes
} from '@material-ui/core/';
import { Brightness4, Brightness7, ContactMail } from '@material-ui/icons/';
import AboutMe from './AboutMe/AboutMe';
import HomePage from './HomePage/HomePage';
import Education from './Resume/Components/Education/Education';
import Experience from './Resume/Components/Experience/Experience';
import Skills from './Resume/Components/Skills/Skills';

const useStyles = makeStyles(() => ({
  container: {
    height: '100%',
    width: '100%',
    overflow: 'auto',
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1
  },
  fab: {
    justifyContent: 'center'
  }
}));

function App(): React.ReactElement {
  const classes = useStyles();

  const [darkState, setDarkState] = useState(false);
  const palletType = darkState ? 'dark' : 'light';
  let theme = createMuiTheme({
    palette: {
      primary: {
        light: '#424242',
        main: '#424242',
        dark: '#424242',
        contrastText: '#fff'
      },
      secondary: {
        light: '#2196f3',
        main: '#2196f3',
        dark: '#2196f3',
        contrastText: '#000'
      },
      type: palletType
    }
  });

  theme = responsiveFontSizes(theme);

  const handleThemeChange = () => {
    setDarkState(!darkState);
  };

  const scrollToRef = (ref: React.RefObject<HTMLDivElement>) => {
    if (ref.current !== null) {
      window.scrollTo({ behavior: 'smooth', top: ref.current.offsetTop });
    }
  };

  const homeRef = useRef<HTMLDivElement | null>(null);
  const aboutRef = useRef<HTMLDivElement | null>(null);
  const educationRef = useRef<HTMLDivElement | null>(null);
  const experienceRef = useRef<HTMLDivElement | null>(null);
  const skillsRef = useRef<HTMLDivElement | null>(null);

  return (
    <MuiThemeProvider theme={theme}>
      <Paper square elevation={0} className={classes.container}>
        <AppBar
          id="appHeader"
          position="fixed"
          color={theme.palette.type === 'dark' ? 'primary' : 'secondary'}
        >
          <Toolbar>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Button href="#home" onClick={() => scrollToRef(homeRef)}>
                Home
              </Button>
              <Button href="#about" onClick={() => scrollToRef(aboutRef)}>
                About
              </Button>
              <Button
                href="#education"
                onClick={() => scrollToRef(educationRef)}
              >
                Education
              </Button>
              <Button
                href="#experience"
                onClick={() => scrollToRef(experienceRef)}
              >
                Experience
              </Button>
              <Button href="#skills" onClick={() => scrollToRef(skillsRef)}>
                Skills
              </Button>
            </Grid>
            <Box flexGrow={1} />
            <Tooltip title="Contact Me">
              <IconButton color="inherit" href="mailto: mqpham@syr.edu">
                <ContactMail />
              </IconButton>
            </Tooltip>
            <Tooltip
              title={
                theme.palette.type === 'dark'
                  ? 'Toggle Light Mode'
                  : 'Toggle Dark Mode'
              }
            >
              <IconButton color="inherit" onClick={handleThemeChange}>
                {theme.palette.type === 'dark' ? (
                  <Brightness7 />
                ) : (
                  <Brightness4 />
                )}
              </IconButton>
            </Tooltip>
          </Toolbar>
        </AppBar>
        <div ref={homeRef}>
          <HomePage />
        </div>
        <Divider />
        <div ref={aboutRef}>
          <AboutMe />
        </div>
        <Divider />
        <div ref={educationRef}>
          <Education />
        </div>
        <Divider />
        <div ref={experienceRef}>
          <Experience />
        </div>
        <Divider />
        <div ref={skillsRef}>
          <Skills />
        </div>
        <Divider />
      </Paper>
    </MuiThemeProvider>
  );
}

export default App;
