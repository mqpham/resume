import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from './App';

describe('<App />', () => {
  window.scrollTo = jest.fn();

  it('should render without error', () => {
    render(<App />);

    expect(screen.getByText('Home')).not.toBeNull();
  });

  it('should click on the home button', () => {
    render(<App />);

    userEvent.click(screen.getByText('Home'));

    expect(window.scrollTo).toHaveBeenCalledTimes(1);
  });

  it('should click on the about button', () => {
    render(<App />);

    userEvent.click(screen.getByText('About'));

    expect(window.scrollTo).toHaveBeenCalledTimes(2);
  });

  it('should click on the education button', () => {
    render(<App />);

    userEvent.click(screen.getAllByText('Education')[0]);

    expect(window.scrollTo).toHaveBeenCalledTimes(3);
  });

  it('should click on the experience button', () => {
    render(<App />);

    userEvent.click(screen.getAllByText('Experience')[0]);

    expect(window.scrollTo).toHaveBeenCalledTimes(4);
  });

  it('should click on the skills button', () => {
    render(<App />);

    userEvent.click(screen.getAllByText('Skills')[0]);

    expect(window.scrollTo).toHaveBeenCalledTimes(5);
  });

  it('should toggle dark mode', () => {
    render(<App />);

    const appHeader = document.getElementById('appHeader');

    if (!appHeader) {
      throw new Error('Could not find appHeader');
    }

    userEvent.click(screen.getByRole('button', { name: 'Toggle Dark Mode' }));

    expect(appHeader.className).toContain('colorPrimary');
  });

  it('should show in responsive layout', () => {
    window.matchMedia = window.matchMedia || function () {
      return {
        matches: true,
        addListener() {},
        removeListener() {},
      };
    };

    render(<App />);

    expect(screen.getByText('Home')).not.toBeNull();
  });
});
